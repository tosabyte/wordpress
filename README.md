# Quick wordpress deployment

Project for quick Wordpress deployment with [Polemarch](https://github.com/vstconsulting/polemarch). 

Supported OS
------------
This project is able to deploy Wordpress on hosts with following OS:

* Ubuntu;
* Debian;
* RedHat;
* CentOS.

Project's playbook
------------------

* **deploy.yml**: This playbook deploys lastest Worpress version on all hosts from
selected inventory.

Form of quick deployment
------------------------
This project's form of quick deployment provides you with 2 fields:

* **mysql user** - name of MySQL user for database of your Worpress site.
* **mysql password** - password of MySQL user for database of your Worpress site.

Enjoy it!